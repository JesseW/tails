[[!meta title="Tails report for September, 2015"]]

FIXME Edito

[[!toc]]

Releases
========

* [[Tails 1.6 was released on September 22, 2015|news/version_1.6]] (minor release).

* The next release (1.7) is [[planned for November 11|https://tails.boum.org/contribute/calendar/]].

Code
====

FIXME

Documentation and website
=========================

FIXME

User experience
===============

FIXME

Infrastructure
==============

* Our test suite covers FIXME scenarios, FIXME more that in April.
  - at the beginning of September: 191
  - at the end of September: 185 (FIXME: ask the developers why if
    needed, after checking the SponsorR and SponsorS reports)

* FIXME more?

Funding
=======

FIXME

Outreach
========

FIXME

Upcoming events
---------------
There will be a Tails and Tor workshop to the [Capitole du libre](https://2015.capitoledulibre.org) in Toulouse (France) on November 22nd. 

FIXME

On-going discussions
====================

FIXME

Press and testimonials
======================
* 2015-09-24: [Open Source Privacy: Tails OS Issues Security Fixes and New Release](http://thevarguy.com/open-source-application-software-companies/092415/open-source-privacy-tails-os-issues-security-fixes-and-ne) by Christopher Tozzi on The VAR Guy.
 
Translation
===========

FIXME

At the end of the month:

All website PO files

  - de: 19% (1283) strings translated, 0% strings fuzzy, 18% words translated
  - fr: 46% (3120) strings translated, 2% strings fuzzy, 43% words translated
  - pt: 27% (1863) strings translated, 3% strings fuzzy, 26% words translated

Total original words: 76959

[[Core PO files|contribute/l10n_tricks/core_po_files.txt]]

  - de: 60% (803) strings translated, 0% strings fuzzy, 68% words translated
  - fr: 92% (1214) strings translated, 3% strings fuzzy, 92% words translated
  - pt: 84% (1119) strings translated, 8% strings fuzzy, 87% words translated

Total original words: 14258

Metrics
=======

* Tails has been started more than FIXME times this month. This makes FIXME boots a day on average.

* FIXME downloads of the OpenPGP signature of Tails ISO from our website.

* FIXME bug reports were received through WhisperBack.

-- Report by BitingBird for Tails folks
